



// Define HTML element references for each city
const mumbaiTemp = document.getElementById("mumbaiTemp");
const londonTemp = document.getElementById("londonTemp");
const tokyoTemp = document.getElementById("tokyoTemp");
const parisTemp = document.getElementById("parisTemp");
const sydneyTemp = document.getElementById("sydneyTemp");
const istanbulTemp = document.getElementById("istanbulTemp");

// Define getweatherBycity function
function getweatherBycity(latitude, longitude, cityElement) {
    // Return a function that fetches weather data for the given latitude and longitude
    return function getWeather() {
        const APIKey = '1abe6ac71361d0ef9853007b38c985be';
        fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${APIKey}&units=metric`)
            .then(response => response.json())
            .then(weather => {
                // Update the corresponding HTML element with the temperature data
                cityElement.innerHTML = weather.main.temp + "&deg; C";
                console.log(weather);
            })
            .catch(error => console.log(error));
    };
}

// Create functions for each city and pass the corresponding HTML element reference
const newYork = getweatherBycity(28.7041, 77.1025, mumbaiTemp);
const London = getweatherBycity(51.5074, -0.1278, londonTemp);
const Tokyo = getweatherBycity(35.6895, 139.6917, tokyoTemp);
const Paris = getweatherBycity(48.8566, 2.3522, parisTemp);
const Sydney = getweatherBycity(-33.8688, 151.2093, sydneyTemp);
const istanbul = getweatherBycity(41.0082, 28.9784, istanbulTemp);


// Call each function to fetch weather data
newYork();
London();
Tokyo();
Paris();
Sydney();
istanbul();

