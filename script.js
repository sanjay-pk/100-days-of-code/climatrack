const btn = document.getElementById('btnClick') 
const temprature = document.getElementById('temp')
const cityName = document.getElementById('cityName')
const cloudStatus = document.getElementById('cloudStatus')
const humidity = document.getElementById('humiditySpan')
const wind = document.getElementById('windSpan')
const weatherError = document.getElementById('error404')
const weatherDisplay = document.getElementById('weatherDisplay')
const backBtn = document.getElementById('backBtn')
const cityInput = document.getElementById('cityInput')


// added click evnet on input button
btn.addEventListener('click', ()=>{
    // api key stores in APIkey
    const APIKey = '1abe6ac71361d0ef9853007b38c985be'
    // get search input 
    const city = document.getElementById('cityInput').value;
    // fetch the api with APIkey and city
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${APIKey}`)
    .then(response => response.json())
    .then((weather)=>{
        //  error condition check. in weather cod found the  404 thats means error
        if (weather.cod === '404') {
            // weather details div none and error div block
            weatherDisplay.style.display = 'none'
            weatherError.style.display = 'flex'
        }
        // input field null after searching
        cityInput.value = '';
        // append the temp form api
        temprature.innerHTML = weather.main.temp + '&deg C'
        // append city name from input field
        cityName.innerHTML = city + ',' + weather.sys.country
        // append cloud status
        cloudStatus.innerHTML = weather.weather[0].description
        // append humidity
        humidity.innerHTML = weather.main.humidity + '%'
        // append wind
        wind.innerHTML = weather.wind.speed + "km/h"
    })
    .catch(error => console.log(error));
})

// when back button pressed then load previous screen
backBtn.addEventListener('click', ()=>{
    weatherError.style.display = 'none'
    weatherDisplay.style.display = 'flex'
    cityInput.value = '';
})

// getting current location and store latitude and longitude cureentWeather function

if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position =>{
          const getWe = CurrentWeather(position.coords.latitude , position.coords.longitude)   
      });
     
    } else {
      console.log('Geolocation is NOT Available');
    }
// when currentWeather calls passing latitude and longitude parameters
function CurrentWeather(latitude,longitude){
    // stored apikey
    const APIKey = '1abe6ac71361d0ef9853007b38c985be'
    // fetching the api and append the parameters
    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${APIKey}&units=metric`)
    .then((response) => response.json())
    .then((deviceWeather)=>{
        // input field null after searching
        cityInput.value = '';
        // append the temp form api
        temprature.innerHTML = deviceWeather.main.temp + '&deg C'
        // append city name from input field
        cityName.innerHTML = deviceWeather.name + ',' + deviceWeather.sys.country
        // append cloud status
        cloudStatus.innerHTML = deviceWeather.weather[0].description
        // append humidity
        humidity.innerHTML = deviceWeather.main.humidity + '%'
        // append wind
        wind.innerHTML = deviceWeather.wind.speed + "km/h"
        console.log(deviceWeather)      
    })
    .catch(error => console.log(error));
}
